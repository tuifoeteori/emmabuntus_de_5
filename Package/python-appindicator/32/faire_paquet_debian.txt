# Outil à installer :
# sudo apt-get install debhelper cdbs lintian build-essential fakeroot devscripts pbuilder dh-make debootstrap
#
# https://www.debian-fr.org/t/extraire-paquet-deb/31870
#
# Pour extraire le paquet :
#
# dpkg-deb -x paquet.deb repertoire -> extrait l'arborescence
# dpkg-deb -e paquet.deb -> extrait le répertoire DEBIAN contenant les différents fichiers postinst, control, etc
#
# Pour assembler le paquet :
#
# dpkg-deb -b repertoire paquet.deb
#

# Méthode paquet origine

version=0.4.92-4_i386
nom_paquet=python-appindicator
ext_paquet=deb

mkdir ${nom_paquet}
dpkg-deb -x ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}
dpkg-deb -e ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}/DEBIAN

# Modifier le fichier DEBIAN/control en mettant libappindicator1 (>= 0.4.92-4) lieu de libappindicator1 (= 0.4.92-4)

# Méthode assemblage

dpkg-deb -b ${nom_paquet} ${nom_paquet}_${version}_mod_emma.${ext_paquet}


