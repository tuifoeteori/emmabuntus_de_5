#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#   Calamares is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Calamares is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Calamares. If not, see <http://www.gnu.org/licenses/>.
#


from libcalamares.utils import target_env_call
from os.path import exists


class CleanupOem:
    def remove_pkg(self, pkg, path):
        if exists(path):
            target_env_call(['apt-get','--purge', '-q', '-y','autoremove', pkg])

    def run(self):
        self.remove_pkg("calamares-settings-debian", "/etc/calamares/branding/debian/branding.desc")
        self.remove_pkg("calamares", "/usr/lib/x86_64-linux-gnu/calamares/libcalamares.so")
        if exists('/etc/calamares'):
            target_env_call(['rm', '-R', '/etc/calamares'])
        if exists('/usr/lib/calamares'):
            target_env_call(['rm', '-R', '/usr/lib/calamares'])
        self.remove_pkg("live-boot", "/usr/bin/live-boot")
        self.remove_pkg("live-config-systemd", "/usr/bin/live-config")
        self.remove_pkg("live-config", "/usr/bin/live-config")
        if exists('/etc/fstab.orig.calamares'):
            target_env_call(['mv', '/etc/fstab.orig.calamares', '/etc/fstab'])
        if exists('/etc/sudoers.d/g_oem'):
            target_env_call(['rm', '/etc/sudoers.d/g_oem'])
        return None


def run():
    """ Cleanup OEM files """

    oem = CleanupOem()

    return oem.run()
