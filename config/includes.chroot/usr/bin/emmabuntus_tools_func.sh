#!/bin/bash


# emmabuntus_tools_func.sh --
#
#   This file used to display a Tools panel for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

repertoire_script=/usr/bin


option=$1
echo "option=${option}"


function FCT_KILL()
{

kill -9 $(ps ax | grep gtkdialog | grep EMMABUNTUS_TOOLS | awk '{print $1}')

}

function FCT_RELOAD()
{

${repertoire_script}/emmabuntus_tools.sh

}

function FCT_WAIT_PROCESS()
{

sleep 2
while ps axg | grep -vw grep | grep -w $1 > /dev/null; do sleep 0.1; done

}

if [[ ${option} == Ventoy ]] ; then
FCT_KILL
/usr/bin/emmabuntus_ventoy.sh
FCT_WAIT_PROCESS emmabuntus_ventoy.sh
FCT_RELOAD
fi

if [[ ${option} == USB-Creator ]] ; then
FCT_KILL
mintstick -m iso
FCT_WAIT_PROCESS mintstick
FCT_RELOAD
fi

if [[ ${option} == USB-Formatter ]] ; then
FCT_KILL
mintstick -m format
FCT_WAIT_PROCESS mintstick
FCT_RELOAD
fi

if [[ ${option} ==  BleachBit ]] ; then
FCT_KILL
pkexec bleachbit
FCT_WAIT_PROCESS bleachbit
FCT_RELOAD
fi

if [[ ${option} == Boot-Repair ]] ; then
FCT_KILL
pkexec boot-repair
FCT_WAIT_PROCESS boot-repair
FCT_RELOAD
fi

if [[ ${option} == OS-Uninstaller ]] ; then
FCT_KILL
pkexec os-uninstaller
FCT_WAIT_PROCESS os-uninstaller
FCT_RELOAD
fi

if [[ ${option} == Remove_languages ]] ; then
FCT_KILL
/usr/bin/remove_language_not_use.sh Init
FCT_WAIT_PROCESS remove_language_not_use
FCT_RELOAD
fi

if [[ ${option} == Disk-Info ]] ; then
FCT_KILL
gnome-disks
FCT_WAIT_PROCESS gnome-disks
FCT_RELOAD
fi

if [[ ${option} == TimeShift ]] ; then
FCT_KILL
timeshift-launcher
FCT_WAIT_PROCESS timeshift-launcher
FCT_RELOAD
fi

if [[ ${option} == Cairo-Dock ]] ; then
pkill -9 cairo-dock
cairo-dock -m &
fi

if [[ ${option} == System-Sound ]] ; then
FCT_KILL
pavucontrol
FCT_WAIT_PROCESS pavucontrol
FCT_RELOAD
fi

if [[ ${option} == ALSA-Mixer ]] ; then
FCT_KILL
x-terminal-emulator -e alsamixer
FCT_WAIT_PROCESS alsamixer
FCT_RELOAD
fi

if [[ ${option} == Fix_tearing ]] ; then
FCT_KILL
/usr/bin/fix_tearing.sh
FCT_WAIT_PROCESS fix_tearing
FCT_RELOAD
fi

if [[ ${option} == Locale ]] ; then
FCT_KILL
/usr/bin/change_language.sh
FCT_WAIT_PROCESS change_language
FCT_RELOAD
fi

if [[ ${option} == Install-no-Free ]] ; then
FCT_KILL
/opt/Install_non_free_softwares/install_logiciel_non_libre_wrapper.sh
FCT_WAIT_PROCESS install_logiciel_non_libre_wrapper.sh
FCT_RELOAD
fi

if [[ ${option} == Config-Printer ]] ; then
FCT_KILL
system-config-printer
FCT_WAIT_PROCESS system-config-p
FCT_RELOAD
fi

if [[ ${option} == HPLip ]] ; then
FCT_KILL
hp-toolbox
FCT_WAIT_PROCESS hp-toolbox
FCT_RELOAD
fi

if [[ ${option} == Brother ]] ; then
FCT_KILL
pkexec /opt/Brother/install.sh
FCT_WAIT_PROCESS /opt/Brother/install.sh
FCT_RELOAD
fi

if [[ ${option} == TurboPrint ]] ; then
FCT_KILL
/opt/Turboprint/turboprint_wrapper.sh
FCT_WAIT_PROCESS turboprint_wrapper.sh
FCT_RELOAD
fi

if [[ ${option} == Flatpak ]] ; then
FCT_KILL
xdg-open https://flathub.org/
FCT_WAIT_PROCESS firefox
FCT_RELOAD
fi

if [[ ${option} == System-Info ]] ; then
FCT_KILL
inxi-gui
FCT_WAIT_PROCESS inxi-gui
FCT_RELOAD
fi

if [[ ${option} == System-Profiler ]] ; then
FCT_KILL
hardinfo
FCT_WAIT_PROCESS hardinfo
FCT_RELOAD
fi

if [[ ${option} == Lxkeymap ]] ; then
FCT_KILL
lxkeymap
FCT_WAIT_PROCESS lxkeymap
FCT_RELOAD
fi

if [[ ${option} == ARandR ]] ; then
FCT_KILL
arandr
FCT_WAIT_PROCESS arandr
FCT_RELOAD
fi

